package Metaheuristico;

// Permite mostrar un mensaje al usuario
public interface IHM {
    void MostrarMensaje(String msg);
}

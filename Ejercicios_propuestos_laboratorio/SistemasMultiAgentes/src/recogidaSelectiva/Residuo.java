package recogidaSelectiva;

import bancoPeces.Objeto;

// Residuos en el entorno
public class Residuo extends Objeto {
    protected final static double DISMINUCION = 0.6;
    
    protected int type;
    protected int tamanio = 1;
    
    public int getType() {
        return type;
    }
    
    public int getTamanio() {
        return tamanio;
    }
    
    public Residuo(double _posX, double _posY, int _tipo) {
        type = _tipo;
        posX = _posX;
        posY = _posY;
    }
    
    public Residuo(Residuo d) {
        posX = d.posX;
        posY = d.posY;
        type = d.type;
    }
    
    public int ZonaInfluencia() {
        return 10 + 8 * (tamanio - 1);
    }
    
    protected void AumentarTamanio() {
        tamanio++;
    }
    
    protected void ReducirTamanio() {
        tamanio--;
    }
    
    protected double ProbaDeTomar() {
        return Math.pow(DISMINUCION, tamanio-1);
    }
}
